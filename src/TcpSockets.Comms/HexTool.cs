﻿using System;
using System.Text;

namespace TcpSockets.Comms
{
    public class HexTool
    {
        public static byte[] GetBytes(string hex)
        {
            // Clean the string 
            hex = hex.Replace("-", "").Replace(" ", "").Replace("\r", "").Replace("\n", "");

            if (hex.Length%2 != 0)
            {
                hex = "0" + hex;
            }

            var numberChars = hex.Length;
            var bytes = new byte[numberChars/2];
            for (var i = 0; i < numberChars; i += 2)
                bytes[i/2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

        public static string GetFormattedString(byte[] data)
        {
            var sb = new StringBuilder();
            var i = 1;
            foreach (var b in data)
            {
                //sb.Append(Conversion.Hex(b));
                //sb.Append(b.ToString("X2"));
                sb.AppendFormat("{0:X2}", b);
                if (i%16 == 0)
                {
                    sb.AppendLine();
                }
                else if (i%4 == 0)
                {
                    sb.Append(" ");
                }
                i++;
            }
            return sb.ToString().Trim();
        }

        public static string GetString(byte[] data)
        {
            var sb = new StringBuilder();
            foreach (var b in data)
            {
                sb.AppendFormat("{0:X2}", b);
            }
            return sb.ToString().Trim();
        }

        public static string GetString(byte b)
        {
            return string.Format("{0:X2}", b);
        }
    }
}