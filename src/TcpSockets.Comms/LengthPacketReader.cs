﻿using System;
using Nito.Async;
using Nito.Async.Sockets;

namespace TcpSockets.Comms
{
    public enum HeaderLength
    {
        Bytes2,
        Bytes4
    }

    public enum ByteOrder
    {
        LittleEndian,
        BigEndian
    }

    public enum HeaderFormatType
    {
        Byte2
    }

    public class HeaderFormat
    {
        // How many bytes is the header
        public HeaderLength Length;
        // Order of bytes 
        public ByteOrder Order;
        // Does the length include the header length 
        public bool IncludeLength;
    }

    /// <summary>
    /// Maintains the necessary buffers for applying a packet protocol over a stream-based socket.
    /// </summary>
    /// <remarks>
    /// <para>This class uses a 4-byte signed integer length prefix, which allows for packet sizes up to 2 GB with single-bit error detection.</para>
    /// <para>Keepalive packets are supported as packets with a length prefix of 0; <see cref="PacketArrived"/> is never called when keepalive packets are returned.</para>
    /// <para>Once <see cref="Start"/> is called, this class continuously reads from the underlying socket, calling <see cref="PacketArrived"/> when packets are received. To stop reading, close the underlying socket.</para>
    /// <para>Reading will also automatically stop when a read error or graceful close is detected.</para>
    /// </remarks>
    public class LengthPacketReader
    {

        private readonly HeaderFormat _headerFormat;

        //private readonly HeaderLength _headerLength;
        //private readonly ByteOrder _byteOrder;

        /// <summary>
        /// The buffer for the length prefix; this is always 4 bytes long.
        /// </summary>
        private readonly byte[] _lengthBuffer;

        /// <summary>
        /// The buffer for the data; this is null if we are receiving the length prefix buffer.
        /// </summary>
        private byte[] _dataBuffer;

        /// <summary>
        /// The number of bytes already read into the buffer (the length buffer if <see cref="_dataBuffer"/> is null, otherwise the data buffer).
        /// </summary>
        private int _bytesReceived;

        /// <summary>
        /// Indicates the completion of a packet read from the socket.
        /// </summary>
        /// <remarks>
        /// <para>This may be called with a null packet, indicating that the other end graciously closed the connection.</para>
        /// </remarks>
        public event Action<AsyncResultEventArgs<byte[]>> PacketArrived;

        /// <summary>
        /// Gets the socket used for communication.
        /// </summary>
        public IAsyncTcpConnection Socket { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="LengthPacketReader"/> class bound to a given socket connection.
        /// </summary>
        /// <param name="socket">The socket used for communication.</param>
        /// <param name="headerLength">How many bytes to use for length</param>
        /// <param name="byteOrder">Byte order of the length.</param>
        public LengthPacketReader(IAsyncTcpConnection socket, HeaderLength headerLength = HeaderLength.Bytes2,
            ByteOrder byteOrder = ByteOrder.BigEndian)
        {
            Socket = socket;

            switch (headerLength)
            {
                case HeaderLength.Bytes2:
                    _lengthBuffer = new byte[2];
                    break;
                case HeaderLength.Bytes4:
                    _lengthBuffer = new byte[4];
                    break;
            }

            _headerFormat = new HeaderFormat {IncludeLength = false, Length = headerLength, Order = byteOrder};
        }

        /// <overloads>
        /// <summary>Sends a packet to a socket.</summary>
        /// <remarks>
        /// <para>Generates a length prefix for the packet and writes the length prefix and packet to the socket.</para>
        /// </remarks>
        /// </overloads>
        /// <summary>Sends a packet to a socket.</summary>
        /// <remarks>
        /// <para>Generates a length prefix for the packet and writes the length prefix and packet to the socket.</para>
        /// </remarks>
        /// <param name="socket">The socket used for communication.</param>
        /// <param name="packet">The packet to send.</param>
        /// <param name="state">The user-defined state that is passed to WriteCompleted. May be null.</param>
        public void WritePacketAsync(IAsyncTcpConnection socket, byte[] packet, object state)
        {
            // Get the length prefix for the message
            var lengthPrefix = CalculateLength(packet.Length, _headerFormat.Order, _headerFormat.Length,
                _headerFormat.IncludeLength);

            var packetWithLength = new byte[lengthPrefix.Length + packet.Length];
            Buffer.BlockCopy(lengthPrefix, 0, packetWithLength, 0, lengthPrefix.Length);
            Buffer.BlockCopy(packet, 0, packetWithLength, lengthPrefix.Length, packet.Length);

            // Send the actual message, this time enabling the normal callback.
            socket.WriteAsync(packetWithLength, state);
        }

        /// <inheritdoc cref="WritePacketAsync(IAsyncTcpConnection, byte[], object)" />
        /// <param name="socket">The socket used for communication.</param>
        /// <param name="packet">The packet to send.</param>
        public void WritePacketAsync(IAsyncTcpConnection socket, byte[] packet)
        {
            WritePacketAsync(socket, packet, null);
        }

        /// <summary>
        /// Sends a keepalive (0-length) packet to the socket.
        /// </summary>
        /// <param name="socket">The socket used for communication.</param>
        public void WriteKeepaliveAsync(IAsyncTcpConnection socket)
        {
            // We use CallbackOnErrorsOnly to indicate that the WriteCompleted callback should only be
            //  called if there was an error.
            if (_headerFormat.Length == HeaderLength.Bytes2)
            {
                // We've gotten the length buffer
                //length = BitConverter.ToInt16(this.lengthBuffer, 0);
                socket.WriteAsync(BitConverter.GetBytes((short) 0), new CallbackOnErrorsOnly());
            }
            else
            {
                //length = BitConverter.ToInt32(this.lengthBuffer, 0);
                socket.WriteAsync(BitConverter.GetBytes((int) 0), new CallbackOnErrorsOnly());
            }
        }

        /// <summary>
        /// Begins reading from the socket.
        /// </summary>
        public void Start()
        {
            Socket.ReadCompleted += SocketReadCompleted;
            ContinueReading();
        }

        /// <summary>
        /// Requests a read directly into the correct buffer.
        /// </summary>
        private void ContinueReading()
        {
            // Read into the appropriate buffer: length or data
            if (_dataBuffer != null)
            {
                Socket.ReadAsync(_dataBuffer, _bytesReceived, _dataBuffer.Length - _bytesReceived);
            }
            else
            {
                Socket.ReadAsync(_lengthBuffer, _bytesReceived, _lengthBuffer.Length - _bytesReceived);
            }
        }

        /// <summary>
        /// Called when a socket read completes. Parses the received data and calls <see cref="PacketArrived"/> if necessary.
        /// </summary>
        /// <param name="e">Argument object containing the number of bytes read.</param>
        /// <exception cref="System.IO.InvalidDataException">If the data received is not a packet.</exception>
        private void SocketReadCompleted(AsyncResultEventArgs<int> e)
        {
            // Pass along read errors verbatim
            if (e.Error != null)
            {
                if (PacketArrived != null)
                {
                    PacketArrived(new AsyncResultEventArgs<byte[]>(e.Error));
                }

                return;
            }

            // Get the number of bytes read into the buffer
            _bytesReceived += e.Result;

            // If we get a zero-length read, then that indicates the remote side graciously closed the connection
            if (e.Result == 0)
            {
                if (PacketArrived != null)
                {
                    PacketArrived(new AsyncResultEventArgs<byte[]>((byte[]) null));
                }

                return;
            }

            if (_dataBuffer == null)
            {
                // (We're currently receiving the length buffer)
                if (_bytesReceived != _lengthBuffer.Length)
                {
                    // We haven't gotten all the length buffer yet
                    ContinueReading();
                }
                else
                {
                    var length = CalculateLength(_lengthBuffer, _headerFormat.Order, _headerFormat.Length,
                        _headerFormat.IncludeLength);

                    // Sanity check for length < 0
                    //  This check will catch 50% of transmission errors that make it past both the IP and Ethernet checksums
                    if (length < 0)
                    {
                        if (PacketArrived != null)
                        {
                            PacketArrived(
                                new AsyncResultEventArgs<byte[]>(
                                    new System.IO.InvalidDataException(
                                        "Packet length less than zero (corrupted message)")));
                        }

                        return;
                    }

                    // Zero-length packets are allowed as keepalives
                    if (length == 0)
                    {
                        _bytesReceived = 0;
                        ContinueReading();
                    }
                    else
                    {
                        // Create the data buffer and start reading into it
                        _dataBuffer = new byte[length];
                        _bytesReceived = 0;
                        ContinueReading();
                    }
                }
            }
            else
            {
                if (_bytesReceived != _dataBuffer.Length)
                {
                    // We haven't gotten all the data buffer yet
                    ContinueReading();
                }
                else
                {
                    // We've gotten an entire packet
                    if (PacketArrived != null)
                    {
                        PacketArrived(new AsyncResultEventArgs<byte[]>(_dataBuffer));
                    }

                    // Start reading the length buffer again
                    _dataBuffer = null;
                    _bytesReceived = 0;
                    ContinueReading();
                }
            }
        }

        public static int CalculateLength(byte[] buffer, ByteOrder byteOrder, HeaderLength headerLength,
            bool includeHeader = false)
        {
            if (byteOrder == ByteOrder.BigEndian)
            {
                Array.Reverse(buffer);
            }

            int length;
            if (headerLength == HeaderLength.Bytes2)
            {
                // We've gotten the length buffer
                length = BitConverter.ToInt16(buffer, 0);
                if (includeHeader) length += 2;
            }
            else
            {
                length = BitConverter.ToInt32(buffer, 0);
                if (includeHeader) length += 4;
            }
            return length;
        }

        public static byte[] CalculateLength(int length, ByteOrder byteOrder, HeaderLength headerLength,
            bool includeHeader = false)
        {
            // Get the length prefix for the message
            byte[] lengthPrefix;

            if (headerLength == HeaderLength.Bytes2)
            {
                if (includeHeader) length += 2;
                lengthPrefix = BitConverter.GetBytes((short) length);
            }
            else
            {
                if (includeHeader) length += 4;
                lengthPrefix = BitConverter.GetBytes(length);
            }

            if (byteOrder == ByteOrder.BigEndian)
            {
                Array.Reverse(lengthPrefix);
            }
            return lengthPrefix;
        }
    }
}