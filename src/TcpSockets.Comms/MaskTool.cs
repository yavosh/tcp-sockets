﻿using System;

namespace TcpSockets.Comms
{
    public enum MaskCardNumberStyle
    {
        Mask4X4,
        Mask6x4,
        PrettyMask4x4,
        PrettyMask6x4,
        ShortMask4x4,
        ShortMask6x4
    }

    public class MaskTool
    {
        public static string MaskString(string value, string mask_char, int start, int end, bool repeat_mask)
        {
            var result = "";

            if (string.IsNullOrWhiteSpace(value)) return value;

            if (start > value.Length || end > value.Length)
            {
                throw new ArgumentException("Value is less than start or end value");
            }

            if (start >= end)
            {
                throw new ArgumentException("Start must be smaller than end");
            }

            if (repeat_mask && mask_char.Length > 1)
            {
                throw new ArgumentException("When repeat is used the mask must be one character long");
            }

            if (repeat_mask)
            {
                var c = mask_char[0];
                mask_char = mask_char.PadLeft(end - start, c);
            }

            result = string.Format("{0}{1}{2}", value.Substring(0, start), mask_char, value.Substring(end));
            return result;
        }

        public static string MaskCardNumber(string card_number)
        {
            return MaskCardNumber(card_number, MaskCardNumberStyle.Mask4X4);
        }

        public static string MaskCardNumber(string card_number, MaskCardNumberStyle style)
        {
            string result;
            switch (style)
            {
                case MaskCardNumberStyle.Mask4X4:
                    result = MaskString(card_number, "********", 4, 12, false);
                    break;
                case MaskCardNumberStyle.ShortMask4x4:
                    result = MaskString(card_number, "*", 4, 12, false);
                    break;
                case MaskCardNumberStyle.PrettyMask4x4:
                    result = MaskString(card_number, "-(*)-", 4, 12, false);
                    break;
                case MaskCardNumberStyle.Mask6x4:
                    result = MaskString(card_number, "******", 6, 12, false);
                    break;
                case MaskCardNumberStyle.PrettyMask6x4:
                    result = MaskString(card_number, "-(*)-", 6, 12, false);
                    break;
                case MaskCardNumberStyle.ShortMask6x4:
                    result = MaskString(card_number, "*", 6, 12, false);
                    break;
                default:
                    result = MaskString(card_number, "********", 4, 12, false);
                    break;
            }


            return result;
        }
    }
}