﻿using System;
using System.ComponentModel;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Nito.Async;
using Nito.Async.Sockets;
using NLog;

namespace TcpSockets.Comms
{
    public class BaseNetworkChannel
    {
        public const int SocketDisconnectErrorCode = 10054;

        public const int CONNECT = 0;
        public const int TX = 1;
        public const int RX = 2;
        public const int ERR = 3;
        public const int ERRCURR = 4;

        public const int CountersLen = 5;

        private IAsyncTcpConnection _clientTcpSocket;

        private Action _keepAliveAction;

        private Timer _keepaliveTimer;
        private LengthPacketReader _packetReader;
        private Timer _reconnectTimer;
        private ServerTcpSocket _serverTcpSocket;

        private ActionThread _workThread;

        protected ByteOrder ByteOrder = ByteOrder.BigEndian;

        protected int[] Counters;
        protected HeaderLength HeaderLength = HeaderLength.Bytes2;

        protected IPEndPoint IpEndpoint;
        protected bool IsServer = false;
        protected int KeepaliveIntervalSec = 60;

        protected Logger Logger;

        protected int ReconnectIntervalSec = 10;

        public BaseNetworkChannel()
        {
            Counters = new int[CountersLen];
            Logger = LogManager.GetLogger(GetType().FullName);
        }

        public string Id { get; set; }

        public bool IsConnected
        {
            get { return _packetReader != null; }
        }

        public event Action<IPEndPoint> OnConnected;
        public event Action<string> OnDisconnected;
        public event Action<string, string> OnMessage;

        public virtual void SetByteOrder(ByteOrder order)
        {
            ByteOrder = order;
        }

        public virtual void SetHeaderLength(HeaderLength length)
        {
            HeaderLength = length;
        }

        protected virtual void PacketArrived(AsyncResultEventArgs<byte[]> obj)
        {
            try
            {
                // Check for errors
                if (obj.Error != null)
                {
                    var ex = obj.Error as SocketException;
                    if (ex != null)
                    {
                        if (ex.ErrorCode == SocketDisconnectErrorCode)
                        {
                            Logger.Trace("Client disconnected from server");
                        }
                        else
                        {
                            Logger.Error(obj.Error, "Socket exception - [{0}] {1}", ex.ErrorCode, ex.Message);
                        }
                    }
                    else
                    {
                        Logger.Error(obj.Error, "Socket packet error - " + obj.Error.Message);
                    }
                    ResetSocket(_clientTcpSocket);
                }
                else if (obj.Result == null)
                {
                    // PacketArrived completes with a null packet when the other side gracefully closes the connection
                    Logger.Warn("Socket graceful close detected");

                    // Close the socket and handle the state transition to disconnected.
                    ResetSocket(_clientTcpSocket);
                }
                else
                {
                    Logger.Trace("Packet arrived len:{0} -> \r\n{1}", obj.Result.Length,
                        HexTool.GetFormattedString(obj.Result));
                    if (OnMessage != null)
                    {
                        //Decode mesage will return a null message if the message is not parsable
                        //ex: Rejected vap messages
                        var message = DecodeMessage(obj.Result);

                        if (message == null)
                        {
                            Logger.Error("Message could not be decoded");
                            return;
                        }

                        OnMessage.Invoke(message, ToString());
                        Counters[RX]++;
                    }
                }

                Counters[ERRCURR] = 0;
            }
            catch (Exception ex)
            {
                Counters[ERRCURR]++;
                Counters[ERR]++;
                Logger.Error(ex, "Socket packet arrive error - " + ex.Message);
                ResetSocket(_clientTcpSocket);
            }
        }

        public virtual void Send(string m)
        {
            if (_packetReader == null)
            {
                throw new ChannelException("Cannot send, channel is not connected");
            }

            try
            {
                Logger.Debug("Sending message {0}", m);

                var data = EncodeMessage(m);


                var header = GetMessageHeader(m, data.Length);
                var trailer = GetMessageTrailler(m, data.Length);
                var payloadLength = header.Length + data.Length + trailer.Length;
                var length = CalculateLength(payloadLength);
                var payload = new byte[length.Length + header.Length + data.Length + trailer.Length];

                var position = 0;
                Buffer.BlockCopy(length, 0, payload, 0, length.Length);
                position += length.Length;
                if (header.Length > 0)
                {
                    Buffer.BlockCopy(header, 0, payload, position, header.Length);
                    position += header.Length;
                }

                Buffer.BlockCopy(data, 0, payload, position, data.Length);

                if (trailer.Length > 0)
                {
                    Buffer.BlockCopy(trailer, 0, payload, position, trailer.Length);
                }

                SendMessageData(payload);
                Counters[ERRCURR] = 0;
                Counters[TX]++;
            }
            catch (Exception ex)
            {
                Counters[ERR]++;
                Counters[ERRCURR]++;
                Logger.Error(ex, "Error sending message: {0} - {1}", m, ex.Message);
                throw;
            }
        }

        protected byte[] CalculateLength(int length)
        {
            return LengthPacketReader.CalculateLength(length, ByteOrder, HeaderLength);
        }

        protected int CalculateLength(byte[] length)
        {
            return LengthPacketReader.CalculateLength(length, ByteOrder, HeaderLength);
        }

        protected virtual string DecodeMessage(byte[] data)
        {
            return Encoding.UTF8.GetString(data);
        }

        protected virtual byte[] EncodeMessage(string message)
        {
            return Encoding.UTF8.GetBytes(message);
        }

        protected virtual byte[] GetMessageTrailler(string m, int len)
        {
            return new byte[0];
        }

        protected virtual byte[] GetMessageHeader(string m, int len)
        {
            return new byte[0];
        }

        protected void SendMessageData(byte[] data)
        {
            _clientTcpSocket.WriteAsync(data, null);
        }

        protected void DoConnect()
        {
            if (IsServer)
            {
                Logger.Info("Starting server");
                ConnectServer();
            }
            else
            {
                Logger.Info("Starting client");
                ConnectClient();
            }
        }

        public void Connect()
        {
            Logger = LogManager.GetLogger(GetType().FullName + "[id:" + Id + "]");

            if (_workThread != null)
            {
                throw new ChannelException("Cannot connect, connection is already running.");
            }

            if (_keepAliveAction == null)
            {
                _keepAliveAction = () =>
                {
                    if (_packetReader != null)
                    {
                        _packetReader.WriteKeepaliveAsync(_clientTcpSocket);
                    }
                };
            }

            _workThread = new ActionThread();
            _workThread.Start();
            _workThread.Do(() =>
            {
                _keepaliveTimer = new Timer();
                _keepaliveTimer.Elapsed += () => _keepAliveAction();
                if (KeepaliveIntervalSec > 0)
                {
                    _keepaliveTimer.SetPeriodic(TimeSpan.FromSeconds(KeepaliveIntervalSec));
                }

                _reconnectTimer = new Timer();
                _reconnectTimer.Elapsed += ReconnectSocket;
                if (ReconnectIntervalSec > 0)
                {
                    _reconnectTimer.Interval = TimeSpan.FromSeconds(ReconnectIntervalSec);
                }

                DoConnect();
            });
        }

        public void Disconnect()
        {
            Logger.Info(IsServer ? "Stopping server" : "Stopping client");

            if (_workThread != null)
            {
                _workThread.Join(TimeSpan.FromMilliseconds(1000));
                _workThread = null;
            }

            if (_clientTcpSocket != null)
            {
                _clientTcpSocket.Close();
                _clientTcpSocket = null;
            }

            if (_serverTcpSocket != null)
            {
                _serverTcpSocket.Close();
                _serverTcpSocket = null;
            }

            if (_keepaliveTimer != null) _keepaliveTimer.Cancel();
            if (_reconnectTimer != null) _reconnectTimer.Cancel();

            _packetReader = null;

            if (OnDisconnected != null) OnDisconnected(Id);

            ResetCounters();
        }

        private void ResetCounters()
        {
            Logger.Debug("Reset counters, old counters\r\n{0}", CountersAsString());
            Counters = new int[CountersLen];
        }

        public void Reconnect()
        {
            Disconnect();
            Connect();
        }

        private void ConnectServer()
        {
            Logger.Info("Server will listen on {0}", IpEndpoint);
            _serverTcpSocket = new ServerTcpSocket();
            _serverTcpSocket.Bind(IpEndpoint, 10);
            _serverTcpSocket.AcceptCompleted += ServerClientConnected;
            _serverTcpSocket.AcceptAsync();
        }

        protected virtual void ServerClientConnected(AsyncResultEventArgs<ServerChildTcpSocket> childSocketArgs)
        {
            if (childSocketArgs.Cancelled)
            {
                Logger.Warn("Connection from {0} was cancelled", childSocketArgs.Result.LocalEndPoint);
                return;
            }

            if (childSocketArgs.Error != null)
            {
                Counters[ERR]++;
                Counters[ERRCURR]++;
                Logger.Error(childSocketArgs.Error, "Error connecting from {0}", childSocketArgs.Result.LocalEndPoint);
                return;
            }

            if (_clientTcpSocket != null)
            {
                Counters[ERR]++;
                Counters[ERRCURR]++;
                Logger.Error("Error accepting client connection, client is already connected");
                childSocketArgs.Result.AbortiveClose();
                return;
            }

            _clientTcpSocket = childSocketArgs.Result;
            _clientTcpSocket.WriteCompleted += ClientSocketWriteCompleted;
            _clientTcpSocket.ShutdownCompleted += ClientSocketShutdownCompleted;

            _packetReader = new LengthPacketReader(_clientTcpSocket, HeaderLength, ByteOrder);
            _packetReader.PacketArrived += obj => PacketArrived(obj);

            Logger.Info("Connection established to {0} => {1}", Id, _clientTcpSocket.RemoteEndPoint);
            Logger.Info("Creating PacketProtocol HeaderLength:{0} ByteOrder:{1}", HeaderLength, ByteOrder);

            _packetReader.Start();

            if (OnConnected != null)
            {
                OnConnected(_clientTcpSocket.LocalEndPoint);
            }

            Counters[ERRCURR] = 0;
            Counters[CONNECT]++;
            _serverTcpSocket.AcceptAsync();
        }

        private void ConnectClient()
        {
            var socket = new ClientTcpSocket();
            socket.ConnectCompleted += ClientConnected;
            socket.WriteCompleted += ClientSocketWriteCompleted;
            socket.ShutdownCompleted += ClientSocketShutdownCompleted;

            Logger.Info("Client will connect to {0}", IpEndpoint);
            Logger.Info("Attempting connection for {0} => {1}", Id, IpEndpoint);

            socket.ConnectAsync(IpEndpoint);
            _clientTcpSocket = socket;
        }

        private void ClientConnected(AsyncCompletedEventArgs obj)
        {
            try
            {
                if (obj.Error != null)
                {
                    // log error
                    Logger.Error(obj.Error, "Error - " + obj.Error.Message);
                    ResetSocket(_clientTcpSocket);
                    return;
                }

                Logger.Info("Connected to {0}", _clientTcpSocket.RemoteEndPoint);
                _packetReader = new LengthPacketReader(_clientTcpSocket, HeaderLength, ByteOrder);
                _packetReader.PacketArrived += obj1 => PacketArrived(obj1);
                Logger.Info("Creating PacketProtocol HeaderLength:{0} ByteOrder:{1}", HeaderLength, ByteOrder);

                // Start reading
                _packetReader.Start();

                // Start writing keepalive packets as necessary
                _keepaliveTimer.SetPeriodic(_keepaliveTimer.Interval);

                if (OnConnected != null)
                {
                    OnConnected(_clientTcpSocket.RemoteEndPoint);
                }

                Counters[ERRCURR] = 0;
                Counters[CONNECT]++;
            }
            catch (Exception ex)
            {
                Counters[ERR]++;
                Counters[ERRCURR]++;

                ResetSocket(_clientTcpSocket);
                Logger.Error(ex, "Error - " + ex.Message);
            }
        }

        private void ClientSocketShutdownCompleted(AsyncCompletedEventArgs obj)
        {
            // Check for errors
            if (obj.Error != null)
            {
                Logger.Error(obj.Error, "Socket shutdown error - " + obj.Error.Message);
                ResetSocket(_clientTcpSocket);
            }
            else
            {
                Logger.Warn("Socket shutdown completed");
                // Close the socket and set the socket state
                ResetSocket(_clientTcpSocket);
            }
        }

        private void ClientSocketWriteCompleted(AsyncCompletedEventArgs obj)
        {
            // Check for errors
            if (obj.Error != null)
            {
                // Note: WriteCompleted may be called as the result of a normal write (SocketPacketizer.WritePacketAsync),
                //  or as the result of a call to SocketPacketizer.WriteKeepaliveAsync. However, WriteKeepaliveAsync
                //  will never invoke WriteCompleted if the write was successful; it will only invoke WriteCompleted if
                //  the keepalive packet failed (indicating a loss of connection).

                Logger.Error(obj.Error, "Socket write error - " + obj.Error.Message);
                ResetSocket(_clientTcpSocket);
            }
        }

        public void Close()
        {
            Logger.Debug("Socket Close");
            //StateChange(SocketState.Closed);
            _keepaliveTimer.Cancel();
            _clientTcpSocket.Close();
            if (IsServer)
            {
                _serverTcpSocket.Close();
            }

            _reconnectTimer.SetSingleShot(_reconnectTimer.Interval);
        }

        private void ResetSocket(IAsyncTcpConnection socket)
        {
            if (OnDisconnected != null) OnDisconnected(Id);

            _keepaliveTimer.Cancel();

            if (socket != null)
            {
                // Indicate there is no socket connection
                socket.Close();
            }

            if (IsServer)
            {
                _clientTcpSocket = null;
            }
            else
            {
                if (ReconnectIntervalSec > 0)
                {
                    //If client try to recconect after the specified interval
                    Logger.Info("Connection reset reconnecting in {0}s", _reconnectTimer.Interval.TotalSeconds);
                    _reconnectTimer.SetSingleShot(_reconnectTimer.Interval);
                }
            }
        }

        private void ReconnectSocket()
        {
            Logger.Info("Reconnecting to {0}", IpEndpoint);

            _keepaliveTimer.Cancel();

            _serverTcpSocket = null;
            _clientTcpSocket = null;
            _packetReader = null;

            DoConnect();
        }

        public string CountersAsString()
        {
            var sb = new StringBuilder();
            sb.AppendFormat("Counters {0}", Id);
            sb.AppendLine();
            sb.AppendFormat("Connect: {0}", Counters[CONNECT]);
            sb.AppendLine();
            sb.AppendFormat("TX: {0}", Counters[TX]);
            sb.AppendLine();
            sb.AppendFormat("RX: {0}", Counters[RX]);
            sb.AppendLine();
            sb.AppendFormat("ERR: {0}", Counters[ERR]);
            return sb.ToString();
        }
    }
}