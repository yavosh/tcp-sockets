﻿using System;
using System.Runtime.Serialization;

namespace TcpSockets.Comms
{
    [Serializable]
    public class ChannelException : Exception
    {
        //
        // For guidelines regarding the creation of new exception types, see
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
        // and
        //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
        //

        public ChannelException()
        {
        }

        public ChannelException(string message) : base(message)
        {
        }

        public ChannelException(string format, params string[] args) : base(string.Format(format, args))
        {
        }

        public ChannelException(string message, Exception inner) : base(message, inner)
        {
        }

        protected ChannelException(
            SerializationInfo info,
            StreamingContext context)
            : base(info, context)
        {
        }
    }
}