﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;

namespace TcpSockets.Comms
{
    public class ClientChannel : BaseNetworkChannel
    {

        private static long _idCounter;

        public ClientChannel(string host, int port) : this(host, port, HeaderLength.Bytes4)
        {
        }

        public ClientChannel(string host, int port, HeaderLength length, ByteOrder order = ByteOrder.BigEndian)
        {
            _idCounter++;

            KeepaliveIntervalSec = 15;
           

            Id = _idCounter.ToString();

            IsServer = false;
            ByteOrder = order;
            HeaderLength = length;

            IPAddress ipAddress;

            if (IPAddress.TryParse(host, out ipAddress))
            {
                IpEndpoint = new IPEndPoint(ipAddress, port);
            }
            else
            {
                var hostEntry = Dns.GetHostEntry(host);
                ipAddress = hostEntry.AddressList
                    .FirstOrDefault(i => i.AddressFamily == AddressFamily.InterNetwork);

                if (ipAddress == null)
                {
                    throw new ApplicationException("Error resolving address of " + host);
                }

                IpEndpoint = new IPEndPoint(ipAddress, port);
            }

        }
    }
}