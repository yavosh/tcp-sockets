﻿using System.Security.Cryptography;
using System.Text;

namespace TcpSockets.Comms
{
    public class HashTool
    {
        public static string Sha1(string text)
        {
            var bytes = Encoding.UTF8.GetBytes(text);
            var hashstring = new SHA1Managed();
            var hash = hashstring.ComputeHash(bytes);
            var hashString = string.Empty;
            foreach (var x in hash)
            {
                hashString += string.Format("{0:x2}", x);
            }
            return hashString;
        }

        public static string Sha256(string text)
        {
            var bytes = Encoding.UTF8.GetBytes(text);
            var hashstring = new SHA256Managed();
            var hash = hashstring.ComputeHash(bytes);
            var hashString = string.Empty;
            foreach (var x in hash)
            {
                hashString += string.Format("{0:x2}", x);
            }
            return hashString;
        }
    }
}