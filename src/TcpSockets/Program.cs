﻿using System;
using TcpSockets.Comms;

namespace TcpSockets
{
    class Program
    {
        static void Main(string[] args)
        {

//            ClientChannel channel = new ClientChannel("127.0.0.1", 10000, HeaderLength.Bytes4);
            ClientChannel channel = new ClientChannel("www.google.com", 10000, HeaderLength.Bytes4);
            channel.OnMessage += (s, client) =>
            {
                Console.WriteLine("Got Message: from={0} message={1}", client, s);
            };

            channel.OnConnected += point =>
            {
                var message = "Something";
                channel.Send(message);
            };

            channel.Connect();

            Console.WriteLine("Press enter to exit");
            Console.ReadLine();

            Console.WriteLine("CLosing connection");
            channel.Disconnect();
        }
    }
}
